const shortid = require('shortid');

class Game {
  constructor(width, height, numberOfMines, firstClickX, firstClickY) {
    this.id = shortid.generate();
    this.width = width;
    this.height = height;
    this.numberOfMines = numberOfMines;
    this.firstClickX = firstClickX;
    this.firstClickY = firstClickY;
    this.gagne = false;
    this.perdu = false;
    this.onGameEndListeners = [];

    // TODO si il y a plus de mines dans numberOfMines
    // que de cases - 1, throw une erreur
  }

  createGrid() {
    this.grid = [[], []];
    this.discovered = [[], []];

    // TODO Remplir la grille 'grid' aléatoirement:
    // - de largeur this.width
    // - de hauteur this.height
    // - avec this.numberOfMines mines
    // Une mine est représentée par le caractère 'M'
    // Attention, ne pas placer de mine sur la cellule aux corrdonées :
    // x: this.firstClickX
    // y: this.firstClickY

    // TODO définir le nombre de mines adjacentes
    // Indiquer en chiffre (string) le nombre de mines
    // adjacentes dans la grid

    // TODO remplir la grille découverte 'discovered'
    // avec des caractères '?' pour indiquer qu'aucune
    // case n'a été creusée
  }

  dig(x, y) {
    if (!this.grid) {
      this.createGrid();
    }

    // TODO mettre à jour la grille discovered à partir des
    // cases creusées x et y

    // TODO Creuser de nouveau les cases alentours si elles
    // n'ont aucune mine adjacente

    // TODO mettre à jour la variable this.perdu à true si
    // une mine est présente sur la case x, y

    // TODO mettre à jour la variable this.gagne à true si
    // toutes les cases qui ne contiennent pas de mine
    // ont été découvertes

    if (this.gagne || this.perdu) {
      this.onGameEndListeners.forEach(callback => callback());
      return {
        id: this.id,
        grid: this.grid,
        discovered: this.discovered,
      };
    }

    return {
      id: this.id,
      discovered: this.discovered
    };
  }

  onGameEnd(callback) {
    this.onGameEndListeners.push(callback);
  }
}

module.exports = Game;
