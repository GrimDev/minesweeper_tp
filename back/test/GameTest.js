const Game = require('../model/Game');

const width = 10;
const height = 20;
const numberOfMines = 10;
const firstClickX = 10;
const firstClickY = 10;

const game = new Game(
  width,
  height,
  numberOfMines,
  firstClickX,
  firstClickY,
);

console.log('Game: ', game);

const discovered = game.dig(firstClickX, firstClickY);

console.log('Discovered: ', discovered);
