// Récupère la librairie Express
const express = require('express');

// Récupère l'export de ./api
// Quand require un dossier, il récupère l'export
// du fichier index.js de ce dossier
const api = require('./api');

// Création du serveur express (allez check la doc)
const app = express();
app.use(express.json());

const port = 3000;

// Création d'une route avec la méthode GET
// Affiche que le serveur est en ligne
app.get('/', (req, res) => {
  res.send(`Serveur d'API pour le Démineur v1 online`);
});

api(app);

// Faire écouter le serveur sur un port défini
app.listen(port, () => {
  console.log(`Serveur d'API online : http://localhost:${port}`);
});
