const game = require('./game');

const routes = [
  game,
];

module.exports = (app) => {
  routes.forEach((route) => route(app));
};
