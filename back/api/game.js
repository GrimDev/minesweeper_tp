const GameService = require('../services/Game');

module.exports = (app) => {
  /*
    La route suivante attend en paramètre query
      width: largeur du démineur
      height: largeur du démineur
      numberOfMines: nombre de mines
      firstClickX: coordonée x de la première case cliquée
      firstClickY: coordonée y de la première case cliquée
  */
  app.get('/new-game', (req, res) => {
    res.send(GameService.newGame({
      width: req.query.width,
      height: req.query.height,
      numberOfMines: req.query.numberOfMines,
      firstClickX: req.query.firstClickX,
      firstClickY: req.query.firstClickY,
    }));
  });

  /*
    La route suivante attend en paramètre body
      gameId: identifiant de la game
      x: coordonée x à creuser
      y: coordonée y à creuser
  */
  app.post('/dig', (req, res) => {
    res.send(GameService.dig(
      req.body.gameId, // Game Id
      req.body.x, // X
      req.body.y, // Y
    ));
  });
};
