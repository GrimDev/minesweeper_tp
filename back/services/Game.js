const Game = require("../model/Game");

const games = [];

const newGame = ({
  width,
  height,
  numberOfMines,
  firstClickX,
  firstClickY,
}) => {
  const game = new Game(width, height, numberOfMines, firstClickX, firstClickY);
  games.push(game);

  // TODO utiliser le listener onGameEnd de la classe game
  // pour retirer des games en cours la game terminée

  return game.dig(firstClickX, firstClickY);
};

const findGame = (gameId) => {
  return games.find(({ id }) => id === gameId);
};

const dig = (gameId, x, y) => {
  return findGame(gameId).dig(x, y);
};

module.exports = {
  newGame,
  dig,
};
