import './index.scss';

document.getElementById('start').addEventListener('click', () => {
  const width = document.getElementById('width').value;
  const height = document.getElementById('height').value;
  const numberOfM = document.getElementById('numberOfM').value;

  // TODO Afficher le nombre de mines restantes dans la div id numberOfMines

  // TODO Démarrer un timer et l'afficher la div id timer
  // TODO Mettre à jour le timer toutes les secondes

  // TODO créer la grille graphiquement dans la div grid
  // Le résultat attendu est
  /*
    Avec X = numero de ligne et Y numéro de colonne
    <div id="line-X" class="line">
      <div id="col-Y" class="col">
      </div>
    </div>
    ...

    Dans "col-Y":
    - la laisser vide si nombre = 0 ou non creusée
    - lui donner la class dig si elle a été creusée
    - lui ajouter le nombre s'il est connu (dig)
    - afficher/retirer un drapeau si click droit
  */

  // TODO écouter les clicks gauche sur chaque case puis
  // - récupérer les coordonnées
  // - si premier click = appeler locahost:3000/new-game
  // - sinon = appeler localhost:3000/dig si la case ne contient
  // pas de drapeau

  // TODO Interpréter le résultat du serveur après avoir creusé
  // - Afficher la grille découverte
  // - Si perdu ou gagner, l'afficher et arrêter le timer, bloquer le jeu

  // TODO écouter les clicks droit sur chaque case
  // - afficher/retirer un drapeau dans la case correspondante
  // - Utilisez l'émoji ⚑
});
